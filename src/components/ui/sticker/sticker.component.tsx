import * as React from 'react';

import {Card} from 'primereact/card';

import './sticker.component.css';

interface PropTypes {
  text: string;
  imgUrl: string;
}

class Component extends React.Component<PropTypes> {

  render() {
    const {text, imgUrl} = this.props;

    const img = <img src={imgUrl} width="100%" alt="Card header" />

    return (
      <div className="sticker">
        <Card footer={text} header={img}></Card>
      </div>
    );
  }
}

export const Sticker = Component;
