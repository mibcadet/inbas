import * as React from 'react';
import {SyntheticEvent} from 'react';
import * as emailjs from 'emailjs-com';

import {Card} from 'primereact/card';
import {Button} from 'primereact/button';
import {InputText} from 'primereact/inputtext';
import {Messages} from 'primereact/messages';

interface IFormState {
  [key: string]: any;
  isSending: boolean;
}

const FORM_FIELDS = [
  {fieldName:'firstname', text: 'Vorname'},
  {fieldName:'lastname', text: 'Nachname'},
  {fieldName:'street', text: 'Straße'},
  {fieldName:'postal', text: 'Postleitzahl'},
  {fieldName:'city', text: 'Stadt'},
  {fieldName:'phone', text: 'Telefon'},
  {fieldName:'email', text: 'Email'},
  {fieldName:'message', text: 'Nachricht'},
]

class Component extends React.Component<{}, IFormState> {
  state: IFormState = this.resetForm();

  private messages: Messages | null;

  constructor(props = {}, state: IFormState) {
    super(props, state);
    this.messages= new Messages({});
    this.setState({isSending: false});
  }

  render() {
    const {isSending} = this.state;

    return (
      <form name="contact" onSubmit={this.onSubmit}>
        <Card>
          <div className="p-grid">
            <div className="p-col-1">
              {
                FORM_FIELDS.map((field, index) => {
                  return (
                    <div className="p-col-1" key={index}>
                      <span className="p-float-label">
                        <InputText style={{width: '100%'}} disabled={isSending} id={field.fieldName} value={this.state[field.fieldName]} onChange={(event: SyntheticEvent<HTMLInputElement>) => {
                          this.setState({[field.fieldName]: event.currentTarget.value});
                        }} />
                        <label htmlFor={field.fieldName}>{field.text}</label>
                      </span>
                      <br />
                    </div>
                  );
                })
              }
            </div>
            <div className="p-col-1">
              <Button label="Send" disabled={!!isSending} />
            </div>
            <div className="p-col-1">
              <Messages ref={(el) => this.messages = el}></Messages>
            </div>
          </div>
        </Card>
      </form>
    );
  }

  onSubmit = (event: SyntheticEvent<HTMLFormElement>) => {
    event.preventDefault();
    this.sendMail();
  }

  resetForm() {
    return Object.assign({}, ...FORM_FIELDS.map(field => ({[field.fieldName]: ''})));
  }

  isValid = () => {
    return !FORM_FIELDS.find(field => this.state[field.fieldName] === '');
  }

  sendMail = async () => {
    this.setState({isSending: true});
    try {
      if(!this.isValid()) throw new Error('Invalid form input');
      await emailjs.send('smola','template_Yb8kOuMu', this.state, 'user_eCS4qfTllOj0EMyfMPV4X');
      this.messages && this.messages.show({severity: 'success', summary: 'Erfolg! ', detail: 'Danke für den Kontakt'});
    } catch(err) {
      this.messages && this.messages.show({severity: 'error', summary: 'Scheitern! ', detail: 'Fehler beim Senden der E-Mail'});
      console.log(err);
    }
    this.setState(this.resetForm());
    this.setState({isSending: false});
  }
}

export const ContactForm = Component;
