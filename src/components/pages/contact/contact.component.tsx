import * as React from 'react';
import {GMap} from 'primereact/gmap';

import {Card} from 'primereact/card';

import {Slide} from '../../ui/slide/';
import './contact.component.css';

import {ContactForm} from './contactForm/contactForm.component';

declare var google: any;

const Component: React.FC = () => {

  const [lat, lng] = [53.535640, 10.130730];

  const options = {
    center: {
      lat,
      lng
    },
    zoom: 18
  };

  const overlays = [
    new google.maps.Marker({
      position: {
        lat,
        lng
      }, title:"INSTALBAS"}),
  ];
  return (
    <div className="page">
      <Slide rows={1}
              title={{text:'Kontaktiere uns', position: 'top'}}>
      </Slide>
      <Slide rows={3}
              title={{text:'Besuchen Sie uns hier', position: 'bottom'}}>
        <div className="content">
          <Card>
            <GMap overlays={overlays} options={options} style={{width: '100%', minHeight: '320px'}} />
          </Card>
        </div>
      </Slide>
      <Slide rows={1} cols={1}>
        <ContactForm />
      </Slide>
    </div>
  );
}

export const Contact = Component;
