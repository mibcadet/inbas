import * as React from 'react';
import './App.css';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

import {BrowserRouter as Router, Route} from "react-router-dom";

import {Menu} from './components/navigation/menu';

import {Home} from './components/pages/home';
import {Dsk} from './components/pages/dsk';
import {Contact} from './components/pages/contact';

const Component: React.FC = () => {
  return (
      <Router>
          <div className="rootView">
              <div className="nav">
                  <Menu />
              </div>
              <div className="main">
                  <Route path="/" exact component={Home} />
                  <Route path="/dsk" exact component={Dsk} />
                  <Route path="/contact" exact component={Contact} />
              </div>
              <div className="footer">
                  <div className="info">
                      <h2>INSTALBAS</h2>
                      <ul> Adresse:
                          <li>OSTSTEINBEKER WEG 1C</li>
                          <li>22117 HAMBURG</li>
                          <li>INFO@INSTALBAS.DE</li>
                          <li>Steuernummer: 46/014/03493</li>
                      </ul>
                      <ul>Telefon:
                          <li><a href="tel:004915111531988">+49 15111531988</a></li>
                      </ul>
                  </div>
              </div>
          </div>
      </Router>
  );
}

export const App = Component;
